
# Sassy Electronics

## Technical Aspects
    
|                                   | Version/Info      |
| ---------------------------------:|:-----------------:|
| Angular CLI                       | 11.2.10           |
| Database                          | MySQL             | 
| Node (backend)                    | 12.18.4           |
| Frontend Template => E-Shopper    | [View Template](https://www.free-css.com/assets/files/free-css-templates/preview/page203/e-shopper/)      |

+  Other dependent packages and their versions can be seen on the package.json file.

### Project Setup

#### Install Node and Npm

+ For downloading node visit [Download Node](https://nodejs.org/en/download/).
+ Clone the project from the [Project Repo](https://gitlab.com/bhandarishashi/sassy-electronics.git)
+ Go to the project.
+ Checkout to master branch if any others.
+ Run `npm install` , it will install all the prerequisite packages.
+ The project will be hosted in [Sassy Electronics](https://www.manaramprojects.com/sassy_electronics/)

#### Packages Used by Template

+ Bootstrap v3.0.3
+ Scrollup v2.3.3 [View](https://github.com/markgoodyear/scrollup)
+ jQuery v1.10.2
+ Fontawesome 4.0.3 [View](http://fontawesome.io)

## Project Overview
A New Company has emerged onto the market - a Electronics sales company that exclusively sells electronic products. They want to build a new website to advertise their products online.

### Scope Breakdown
The main agenda for building this website is to showcase the products of the Company to their audience as soon as possible.

##### The Project Requirement has been broken down on the basis of same agenda.

+ Homepage
    - Header + Navigation
    - Feature Product Slider with dynamic Images 
    - Featured Product Listing
    - Copyright Disclaimer

+ Product by Category page (can be accessed through Navigation)
    - Product Card with View Product button
    - Pagination (for pagination function display, only two products has been displayed on the view)
    - Category Menu For Easy Access

+ Product Detail Page
    - Category Menu For Easy Access
    - Product Image
    - Product Attributes and Description
    - Recommended Product based on the Viewed Category.
    - Add to Cart

+ Cart Page
    - List of the Item that has been added to the cart
    - Cart Summary with total cost and total item count.

+ Contact Page
    - Fully Functioning Contact Form


##### The reasons behind the requirement definition

+ The primary agenda of the product is to advertise the products to the customer.

+ For this, the must required features are should be 

    - The list of the products with product card
        + Card contains Product Image, Prodcut Name and View Product button.

    - Classification of the products on the basis of types and view filter according to category.
        + eg. Camera, Mobiles, Laptop, etc

    - Description of the products to provide informations to the customer.
        + Product Name
        + Product Attributes
        + Produt Main Image
        + Other images if available
        + Product Rate
        + Description about the Product

    - Recommendation to the customer according to the product s/he is viewing to attract towards similar products.

    - Tags: New and Sale inorder to attract the customer.

    - Contact Page so that customer can reach out to the Company quickly.

    - Add to Cart feature. 
    This feature has been done to showcase the use of the Node.js. On the next iteration submission of the cart in the form of email to the Company can be quickly added such that order can be received.

+ With the help of these features, Company can advertise itself to the customer and make presence on the market. Contact    Informations on the page helps customer to reach out to the Company.

+ These minimum necessary features has been identified according to the market research of the similar Companies on Electronics E-commerce market. Some of them are
    - Neostore online platform
    - Oliz Store online platform
    - Neptronics online platform
    - Prime Store online platform

### Techincal Aspect of Add to Cart

+ User can send add to cart request from the product detail.

+ System will check if the product exist. If exists, then system will check for cart_id.

    -If cart_id doesn't exit,

        + System will create a random cart_id

    - If cart_id is found

        + System wil check if the requested product item already exist on the cart data or not.

            - If already exist, system will update the object
            - If not, system will insert the product item

+ On response, system will send cart_id to the frontend

+ On frontend,

    - System will store the cart_id in the cookies 
    - System will display cart items on the tabular format with attributes: image, title, sku, quantity and price.
    - System will caculate the total cost and total number of items and display to the user

+ User can remove the Items from the cart.

    - On frontend, alert confirmation is shown to verify the process.

    - On backend

        + cart_id and product_id will be sent from header.
        
        + System will check if the cart_id with that product_id exist. If yes, product will be deleted, else response of prodcut not found will be sent.


### Further help
To get any help or had any queries do contact me via mail [Shashi Bhandari](https://www.gmail.com) 

`email`: shasheebhandari@gmail.com
