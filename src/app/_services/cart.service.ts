import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs/index";

@Injectable({
  providedIn: 'root'
})
export class CartService {

  apiUrl = environment.apiUrl;
  frontendUrl = environment.frontendUrl;
  cartUrl = '';

  constructor(private http: HttpClient) {
    this.cartUrl = this.apiUrl + 'cart/';
  }

  submitCart(data: any): Observable<any> {
    return this.http.post<any>(this.cartUrl, data, {
      observe: 'response'
    });
  }

  //get cart detail
  getCartDetail(id: string): Observable<any> {
    return this.http.get<any>(this.cartUrl + id, {
      observe: 'response'
    });
  }

  //remove cart item
  removeCartItem(cart_id: string, product_id: string): Observable<any> {
    return this.http.delete<any>(this.cartUrl + 'remove/' + cart_id + '/' + product_id, {
      observe: 'response'
    });
  }


}
