import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs/index";

@Injectable({
  providedIn: 'root'
})
export class ContactService {
  apiUrl = environment.apiUrl;
  constructor(private http: HttpClient) { }

  submitMessage(data: any): Observable<any> {
    return this.http.post<any>(this.apiUrl + 'contact', data, {
      observe: 'response'
    });
  }


}
