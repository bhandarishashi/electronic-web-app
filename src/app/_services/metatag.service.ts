import { Injectable } from '@angular/core';
import { Meta, Title } from "@angular/platform-browser";

@Injectable({
  providedIn: 'root'
})
export class MetatagService {

  constructor(private title: Title, private meta: Meta) {
  }

  setTags(title: string) {

    this.title.setTitle(title);
    this.meta.updateTag({ name: 'title', content: title });
    this.meta.updateTag({ property: 'sasyyelectronics:title', content: title });
  }


}
