import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs/index";

@Injectable({
  providedIn: 'root'
})
export default class ProductService {
  apiUrl = environment.apiUrl;
  frontendUrl = environment.frontendUrl;
  productUrl = '';
  constructor(private http: HttpClient) {
    this.productUrl = this.apiUrl + 'product/';
  }

  //get products
  getProducts(): Observable<any> {
    return this.http.get<any>(this.productUrl, {
      observe: 'response'
    });
  }


  //get product detail
  getProductDetail(key: string): Observable<any> {
    return this.http.get<any>(this.productUrl + key, {
      observe: 'response'
    });
  }

}
