import { CartComponent } from './cart/cart.component';
import { Error404Component } from './error/error404/error404.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { CategoryProductsComponent } from './category/category-products/category-products.component';
import { ProductComponent } from './product/product.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';


const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'product/detail/:product_key', component: ProductComponent },
  { path: 'category', component: CategoryProductsComponent },
  { path: 'contact', component: ContactUsComponent },
  { path: 'cart', component: CartComponent, pathMatch: 'full' },
  { path: '404', component: Error404Component, pathMatch: 'full' },
  { path: '**', redirectTo: '404' }

];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    initialNavigation: 'enabled',
    scrollPositionRestoration: 'top'
  }
  )],
  exports: [RouterModule]
})
export class AppRoutingModule { }
