import { NavigationService } from './../../_services/navigation.service';
import { CategoryService } from '../../_services/category.service';
import { MetatagService } from '../../_services/metatag.service';
import { Component, OnInit, Inject, PLATFORM_ID, HostListener } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { isPlatformBrowser } from "@angular/common";

@Component({
  selector: 'app-category-products',
  templateUrl: './category-products.component.html',
  styleUrls: ['./category-products.component.scss']
})
export class CategoryProductsComponent implements OnInit {
  category_key = '';
  products = [
    {
      product_id: '',
      name: '',
      description: '',
      price: '',
      category_key: '',
      image: '',
      tag: '',
      discount_price: ''
    }
  ]
  loader = true;
  currentPage = 1;
  limit = 2;
  pageNumber: number[] = [];
  total_page = 0;
  errorMessage = '';
  showErrMsg = false;
  callMethod = false;
  isParam = false;
  constructor(private activatedRoute: ActivatedRoute, private categoryService: CategoryService, private metaTagService: MetatagService,
    @Inject(PLATFORM_ID) private platformId: Object, private router: Router, private navigation: NavigationService) {
    this.showErrMsg = false;
    activatedRoute.queryParams.subscribe(params => {
      var cPage;
      if (params.page != undefined) {
        cPage = params.page;
        this.currentPage = parseInt(cPage);
      } else {
        this.currentPage = 1;
      }
      this.category_key = params.key;
      if (params.key || params.page == undefined || params.page != undefined)
        this.categoryProductList();

    })


  }

  ngOnInit(): void {

  }

  categoryProductList() {
    this.loader = true;
    this.showErrMsg = false;
    this.pageNumber = [];
    this.categoryService.getCategoryProducts(this.category_key, this.currentPage, this.limit).subscribe(res => {
      this.products = res.body.products.rows;
      this.metaTagService.setTags(res.body.category.name + ' | Sassy Electronics');
      var current_page = 1;
      this.total_page = res.body.total_page;
      for (var i = current_page; i <= res.body.total_page; i++) {
        this.pageNumber.push(i);
      }
      this.loader = false;
    }, error => {
      this.products = [];
      if (error.status == 404) {
        this.showErrMsg = true;
        this.errorMessage = error.error.message;
      }
      this.loader = false;
    })
  }

  nextPage() {
    this.loader = true;
    this.showErrMsg = false;
    if (isPlatformBrowser(this.platformId)) {
      window.scrollTo(0, 0);
    }
    if (this.currentPage < this.total_page) {
      this.currentPage++;
      this.categoryService.getCategoryProducts(this.category_key, this.currentPage, this.limit).subscribe(res => {
        this.router.navigateByUrl('/category?key=' + this.category_key + '&page=' + this.currentPage, { skipLocationChange: false });
        this.products = res.body.products.rows;
        this.loader = false;
      }, error => {
        this.products = [];
        if (error.status == 404) {
          this.showErrMsg = true;
          this.errorMessage = error.error.message;
        }
        this.loader = false;
      })
    } else {
      this.loader = false;
    }

  }

  previousPage() {
    this.loader = true;
    this.showErrMsg = false;
    if (isPlatformBrowser(this.platformId)) {
      window.scrollTo(0, 0);
    }
    if (this.currentPage > 1 && this.currentPage <= this.total_page) {
      this.currentPage--;
      this.categoryService.getCategoryProducts(this.category_key, this.currentPage, this.limit).subscribe(res => {
        this.products = res.body.products.rows;
        this.router.navigateByUrl('/category?key=' + this.category_key + '&page=' + this.currentPage, { skipLocationChange: false });
        this.loader = false;
      }, error => {
        this.products = [];
        if (error.status == 404) {
          this.showErrMsg = true;
          this.errorMessage = error.error.message;
        }
        this.loader = false;
      })
    } else {
      this.loader = false;
    }

  }
  goToPage(page: any) {
    this.loader = true;
    this.showErrMsg = false;
    if (isPlatformBrowser(this.platformId)) {
      window.scrollTo(0, 0);
    }
    this.currentPage = page;
    this.categoryService.getCategoryProducts(this.category_key, page, this.limit).subscribe(res => {
      this.products = res.body.products.rows;
      this.router.navigateByUrl('/category?key=' + this.category_key + '&page=' + this.currentPage, { skipLocationChange: false });
      this.loader = false;
    }, error => {
      this.products = [];
      if (error.status == 404) {
        this.showErrMsg = true;
        this.errorMessage = error.error.message;
      }
      this.loader = false;
    })

  }

  @HostListener('window:popstate', ['$event'])
  onPopState(event: any) {
    this.navigation.back();
  }


}
