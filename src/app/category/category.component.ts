import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CategoryService } from '../_services/category.service';
@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss']
})
export class CategoryComponent implements OnInit {
  categories = [{
    category_id: '',
    name: '',
    category_key: ''
  }];
  loader = true;
  constructor(private categoryService: CategoryService) {
    this.categoryList();
  }

  ngOnInit(): void {
  }

  categoryList() {
    this.categoryService.getCategories().subscribe(res => {
      this.categories = res.body.categories;
      this.loader = false;
    }, err => {
      this.categories = [];
      this.loader = false;
    });
  }

}
