import { Component, OnInit } from '@angular/core';
import { MetatagService } from '../../_services/metatag.service';

@Component({
  selector: 'app-error404',
  templateUrl: './error404.component.html',
  styleUrls: ['./error404.component.scss']
})
export class Error404Component implements OnInit {

  constructor(private metaTagService: MetatagService) { }

  ngOnInit(): void {
    this.metaTagService.setTags('Error | Sassy Electronics');

  }

}
