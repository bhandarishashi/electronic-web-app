export const environment = {
  production: true,
  apiUrl: 'https://manaramprojects.com:3000/',
  frontendUrl: 'https://www.manaramprojects.com/sassy_electronics/',
  cookieDomain: 'localhost'
};
